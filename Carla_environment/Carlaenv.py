import glob
import os
import sys
import cv2
import time
import math
import numpy as np
import random
from PythonAPI.carla.agents.tools.misc import compute_magnitude_angle,get_speed
#from PythonAPI.examples.Carla-ppo.CarlaEnv.planner import compute_route_waypoints

try:
    sys.path.append(glob.glob('C:/Users/pnagaraj/Data/carla_versions/CARLA_0.9.5/PythonAPI/carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass
import carla


#Global variables
SECONDS_PER_EPISODE = 70
IM_WIDTH = 640
IM_HEIGHT = 480
num_actions = 9
shw_img = False
COLLISION_FILTER = [['static.sidewalk', -1], ['static.road', -1], ['vehicle.', 500]]
DISCRETE_ACTIONS = { #throttle,steer, brake
    0: [0.0, 0.0, 0.0],    # Coast
    1: [0.0, -0.5, 0.0],   # Turn Left
    2: [0.0, 0.5, 0.0],    # Turn Right
    3: [1.0, 0.0, 0.0],    # Forward
    4: [-0.5, 0.0, 0.5],   # Brake
    5: [1.0, -0.5, 0.0],   # Bear Left & accelerate
    6: [1.0, 0.5, 0.0],    # Bear Right & accelerate
    7: [-0.5, -0.5, 0.5],  # Bear Left & decelerate
    8: [-0.5, 0.5, 0.5],   # Bear Right & decelerate
}

def get_transform(vehicle_location, angle, d=6.4):
    a = math.radians(angle)
    location = carla.Location(d * math.cos(a), d * math.sin(a), 2.0) + vehicle_location
    return carla.Transform(location, carla.Rotation(yaw=180 + angle, pitch=-15))

def view_angle(world,vehicle):
    spectator = world.get_spectator()
    angle = 90
    while angle < 280:
        timestamp = world.wait_for_tick()
        angle += timestamp.delta_seconds * 60.0
        spectator.set_transform(get_transform(vehicle.get_location(), angle - 90))


# car environment--CARLA
class CarEnv:
    SHOW_CAM = False
    STEER_AMT = 0.80
    im_width = IM_WIDTH
    im_height = IM_HEIGHT
    front_camera = None


    def __init__(self):
        self.client = carla.Client("localhost", 2000)  # connect to server
        self.client.set_timeout(20.0)
        self.world = self.client.get_world()
        #self.world = self.client.load_world('Town03')
        self.blueprint_library = self.world.get_blueprint_library()

    # begining of the environment, every episode to reset
    def reset(self):
        self.collison_hist = []  # to reset/clear
        self.actor_list = []  # to reset/clear
        self.list_lane_marking = []
        #self.prev_image = np.zeros([self.im_height, self.im_width, 6], dtype=np.uint8)
        #self.prev_image.fill(255)
        self.prev_image = None

        # vehicle-actor
        self.model_3 = self.blueprint_library.filter("model3")[0]  # to get tesla model 3
        #self.start_pos = self.world.get_map().get_spawn_points()[4]#to spawn at same location every episode
        self.start_pos = random.choice(self.world.get_map().get_spawn_points()) # to spawn at different locations for every episode
        self.vehicle = self.world.spawn_actor(self.model_3,self.start_pos)
        self.actor_list.append(self.vehicle)
        #print(self.start_pos)
        self.end_pos = carla.Transform(carla.Location(x=50.41062, y=210.906, z=1.8431), carla.Rotation(pitch=0, yaw=-0.142975, roll=0))
        # sensor
        self.world.debug.draw_string(self.end_pos.location, 'END', draw_shadow=False,
                              color=carla.Color(r=255, g=0, b=0), life_time=SECONDS_PER_EPISODE,
                              persistent_lines=True)
        self.rgb_cam = self.blueprint_library.find('sensor.camera.rgb')
        self.rgb_cam.set_attribute("image_size_x", str(self.im_width))
        self.rgb_cam.set_attribute("image_size_y", str(self.im_height))
        self.rgb_cam.set_attribute("fov", "120")
        transform = carla.Transform(carla.Location(x=2.5, z=0.7))
        self.sensor = self.world.spawn_actor(self.rgb_cam, transform, attach_to=self.vehicle)
        self.actor_list.append(self.sensor)
        self.sensor.listen(lambda data: self.process_image(data))

        self.vehicle.apply_control(carla.VehicleControl(throttle=0.0, brake=0.0))
        time.sleep(4)

        # lane invasion sensor
        lane_invasion = self.blueprint_library.find("sensor.other.lane_invasion")
        self.lanesensor = self.world.spawn_actor(lane_invasion, transform, attach_to=self.vehicle)
        self.actor_list.append(self.lanesensor)
        self.lanesensor.listen(lambda events: self.laneprocess(events))

        # collision sensor
        colsensor = self.blueprint_library.find("sensor.other.collision")
        self.colsensor = self.world.spawn_actor(colsensor, transform, attach_to=self.vehicle)
        self.actor_list.append(self.colsensor)
        self.colsensor.listen(lambda event: self.collision_data(event))  # just record the collision events

        view_angle(self.world, self.vehicle)
        while self.front_camera is None:
            time.sleep(0.01)

        self.episode_start = time.time()  # save the start time of the episode
        self.vehicle.apply_control(carla.VehicleControl(throttle=0.0, brake=0.0))

        return self.front_camera

    def encode(self, image):
        prev_image = self.prev_image
        self.prev_image = image
        if prev_image is None:
            prev_image = image
        image = np.concatenate([prev_image, image], axis= 2)
        if shw_img:
            cv2.imshow("", image[:,:,:3])
            cv2.waitKey(1)
        return image[:,:,3]


    def laneprocess(self, events):
        self.list_lane_marking.append(events)

    def collision_data(self, event):
        # What we collided with and what was the impulse
        collision_actor_id = event.other_actor.type_id
        collision_impulse = math.sqrt(
            event.normal_impulse.x ** 2 + event.normal_impulse.y ** 2 + event.normal_impulse.z ** 2)

        # Filter collisions
        for actor_id, impulse in COLLISION_FILTER:
            if actor_id in collision_actor_id and (impulse == -1 or collision_impulse <= impulse):
                return
        self.collison_hist.append(event)

    def process_image(self, image):
        i = np.array(image.raw_data)
        i2 = i.reshape((self.im_height, self.im_width, 4))
        i3 = i2[:, :, :3]
        data = cv2.resize(i3, (self.im_width, self.im_height),interpolation=cv2.INTER_AREA)
        data = (data.astype(np.float32) - 128) / 128
        if self.SHOW_CAM:  # if true it will show thw images captured by camera
            cv2.imshow("", data)
            cv2.waitKey(1)
        self.front_camera = data


    def step(self, action):
        # 9actions
        action = DISCRETE_ACTIONS[int(action)]
        throttle = float(np.clip(action[0], 0, 1))  #  clip is used to clip the interval edges i.e greater than 1 = 1 and lesser than 0 = 0
        brake = float(np.abs(np.clip(action[2], 0, 1)))
        steer = float(np.clip(action[1], -1, 1))
        self.vehicle.apply_control(carla.VehicleControl(throttle=throttle, steer=steer, brake=brake))

        prev_distance, angle1 = compute_magnitude_angle(self.start_pos.location, self.vehicle.get_location(),self.vehicle.get_transform().rotation.yaw)
        current_dist, angle = compute_magnitude_angle(self.vehicle.get_location(), self.end_pos.location,self.vehicle.get_transform().rotation.yaw)
        #print("distance travelled from start location {}, distance to destination {}".format(prev_distance,current_dist))

        speed = get_speed(self.vehicle)
        #print("speed of vehicle: {}km/h".format(int(speed))) #speed given in float.so converting it to int

        # reward calculation based on collision, speed
        # terminating episode if speed less than 1kmhr after 20s
        if time.time() - self.episode_start > 20 and speed < 1.0 :
            done = True

        if len(self.collison_hist) != 0:  # any collison terminate the episode
            done = True
            reward = -200  # -200 or

        if len(self.list_lane_marking) > 0:
            done = False
            reward = -1

        if speed > 10 and speed < 60:
            done = False
            reward = 5
        else:
            done = False
            reward = -1

        if self.episode_start + SECONDS_PER_EPISODE < time.time():  # if episode doesnt complete within 10 seconds, then terminate the episode
            done = True

        if current_dist < 2:
           print('destination reached')
           done = True
           reward = 100

        reward += np.clip(prev_distance -current_dist, -10.0,10.0)
        #return self.encode(self.front_camera), reward, done, None
        return self.front_camera, reward, done, None