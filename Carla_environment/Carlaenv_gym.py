import glob
import os
import sys
import cv2
import time
import math
import random
import gym
from gym.utils import seeding
from PythonAPI.examples.Rl_carla_win.Carla_environment.wrappers import *
from PythonAPI.carla.agents.tools.misc import compute_magnitude_angle,get_speed
from PythonAPI.examples.Rl_carla_win.Carla_environment.planner import compute_route_waypoints

try:
    sys.path.append(glob.glob('C:/Users/pnagaraj/Data/carla_versions/CARLA_0.9.5/PythonAPI/carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass
import carla


#Global variables
SECONDS_PER_EPISODE = 80
IM_WIDTH = 1280
IM_HEIGHT = 720
num_actions = 9
shw_img = False
max_distance = 3.0  # Max distance from center before terminating
target_speed = 20.0
viewer_res = (1280, 720)
obs_res = (1280, 720)
DISCRETE_ACTIONS = { #throttle,steer, brake
    0: [0.0, 0.0, 0.0],    # Coast
    1: [0.0, -0.5, 0.0],   # Turn Left
    2: [0.0, 0.5, 0.0],    # Turn Right
    3: [1.0, 0.0, 0.0],    # Forward
    4: [-0.5, 0.0, 0.5],   # Brake
    5: [1.0, -0.5, 0.0],   # Bear Left & accelerate
    6: [1.0, 0.5, 0.0],    # Bear Right & accelerate
    7: [-0.5, -0.5, 0.5],  # Bear Left & decelerate
    8: [-0.5, 0.5, 0.5],   # Bear Right & decelerate
}
camera_transforms = {
    "spectator": carla.Transform(carla.Location(x=-5.5, z=2.8), carla.Rotation(pitch=-15)),
    "dashboard": carla.Transform(carla.Location(x=1.6, z=1.7)),
    "sensor_location": carla.Transform(carla.Location(x=2.5, z=0.7))
}


# car environment--CARLA
class CarEnv(gym.Env):
    SHOW_CAM = True
    STEER_AMT = 0.80
    im_width = IM_WIDTH
    im_height = IM_HEIGHT
    front_camera = None

    def __init__(self):

        self.synchronous = True
        #set up gym env
        self.seed()
        self.action_space = gym.spaces.Box(np.array([-1, 0]), np.array([1, 1]), dtype=np.float32)  # steer, throttle
        self.observation_space = gym.spaces.Box(low=0.0, high=1.0, shape=(*obs_res, 3), dtype=np.float32)
        self.client = carla.Client("localhost", 2000)  # connect to server
        self.client.set_timeout(20.0)
        #self.world = self.client.get_world()
        self.world = self.client.load_world('Town03')
        self.map = self.world.get_map()
        self.blueprint_library = self.world.get_blueprint_library()

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    # begining of the environment, every episode to reset
    def reset(self):
        self.total_reward = 0.0
        self.step_count = 0
        self.collison_hist = []  # to reset/clear
        self.actor_list = []  # to reset/clear
        self.list_lane_marking = []
        self.lane_detected = []
        self.num_routes_completed = -1
        self.center_lane_deviation = 0.0
        self.distance_traveled = 0.0
        self.current_waypoint_index = 0

        #self.prev_image = np.zeros([self.im_height, self.im_width, 6], dtype=np.uint8)
        #self.prev_image.fill(255)
        self.prev_image = None

        # vehicle-actor
        self.model_3 = self.blueprint_library.filter("model3")[0]  # to get tesla model 3
        #self.model = self.blueprint_library.filter('vehicle.bmw.*')
        self.start_pos = self.map.get_spawn_points()[2]#to spawn at same location every episode
        #self.transformm = random.choice(self.world.get_map().get_spawn_points()[0]) # to spawn at different locations for every episode
        self.vehicle = self.world.spawn_actor(self.model_3,self.start_pos)
        self.actor_list.append(self.vehicle)
        random.seed(1)
        self.start_wp, self.end_wp = [self.map.get_waypoint(spawn.location) for spawn in np.random.choice(self.map.get_spawn_points(), 2, replace=False)]
        #self.start_wp, self.end_wp =  np.random.choice(self.map.generate_waypoints(2), 2, replace=False)
        self.route_waypoints = compute_route_waypoints(self.map, self.start_wp, self.end_wp, resolution=5.0)
        #print("\n route", self.route_waypoints) # tuple (waypoint, roadoption)
        self.vehicle.set_transform(self.start_wp.transform) #to change location of vehicle after spwanning
        self.previous_location = self.vehicle.get_transform().location

        draw_string(self.world,self.end_wp.transform.location, 'END',carla.Color(r=255, g=0, b=0),seconds=SECONDS_PER_EPISODE)
        draw_string(self.world,self.start_wp.transform.location, 'START',carla.Color(r=0, g=255, b=0),seconds=SECONDS_PER_EPISODE)
        """displays the waypoints from start to end"""

        i = 0
        self.waypoint_list = [] # it contains list of carla objects
        while i<len(self.route_waypoints):
            self.waypoint_list.append(self.route_waypoints[i][0].transform.location)
            draw_string(self.world, self.route_waypoints[i][0].transform.location, '*', carla.Color(r=0, g=255, b=0),seconds=SECONDS_PER_EPISODE)
            i +=2
        # sensor
        self.rgb_cam = self.blueprint_library.find('sensor.camera.rgb')
        self.rgb_cam.set_attribute("image_size_x", str(self.im_width))
        self.rgb_cam.set_attribute("image_size_y", str(self.im_height))
        self.rgb_cam.set_attribute("fov", "120")
        self.sensor = self.world.spawn_actor(self.rgb_cam, transform=camera_transforms["dashboard"], attach_to=self.vehicle)
        self.actor_list.append(self.sensor)
        self.sensor.listen(lambda data: self.process_image(data))

        self.vehicle.apply_control(carla.VehicleControl(throttle=0.0, brake=0.0))
        time.sleep(4)

        # lane invasion sensor
        lane_invasion = self.blueprint_library.find("sensor.other.lane_invasion")
        self.lanesensor = self.world.spawn_actor(lane_invasion, transform=camera_transforms["spectator"], attach_to=self.vehicle)
        self.actor_list.append(self.lanesensor)
        self.lanesensor.listen(lambda events: self.laneprocess(events))

        # collision sensor
        colsensor = self.blueprint_library.find("sensor.other.collision")
        self.colsensor = self.world.spawn_actor(colsensor, transform=camera_transforms["sensor_location"], attach_to=self.vehicle)
        self.actor_list.append(self.colsensor)
        self.colsensor.listen(lambda event: self.collision_data(event))  # just record the collision events

        view_angle(self.world, self.vehicle)
        while self.front_camera is None:
            time.sleep(0.01)


        self.episode_start = time.time()  # save the start time of the episode
        self.vehicle.apply_control(carla.VehicleControl(throttle=0.0, brake=0.0))

        return self.front_camera

    def encode(self, image):
        prev_image = self.prev_image
        self.prev_image = image
        if prev_image is None:
            prev_image = image
        image = np.concatenate([prev_image, image], axis= 2)
        if shw_img:
            cv2.imshow("", image[:,:,:3])
            cv2.waitKey(1)
        return image[:,:,3]


    def laneprocess(self, events):
        self.list_lane_marking.append(events)

    def collision_data(self, event):
        self.collison_hist.append(event)

    def process_image(self, image):
        i = np.array(image.raw_data)
        i2 = i.reshape((self.im_height, self.im_width, 4))
        i3 = i2[:, :, :3]
        data = cv2.resize(i3, (self.im_width, self.im_height),interpolation=cv2.INTER_AREA)
        data = (data.astype(np.float32) - 128) / 128
        if self.SHOW_CAM:  # if true it will show thw images captured by camera
            cv2.imshow("", data)
            cv2.waitKey(1)
        self.front_camera = data


    def reward_function(self):

        done = False
        reward = 0
        min_speed = 15.0  # km/h
        max_speed = 25.0  # km/h
        if self.center_lane_deviation>2: reward -= 1
        self.speed = get_speed(self.vehicle)
        if  self.speed < 2: # any collison terminate the episode
            reward -= 20
        if len(self.collison_hist) != 0:
            done = True
            reward -= 100
        if self.current_dist < 1:
            print('destination reached')
            done = True
            reward += 100
        if len(self.list_lane_marking) > 0: reward = -1
        distance_between = self.previous_location.distance(self.waypoint_list[self.i])
        if distance_between < 0.5:
            print('waypoint {} covered'.format(self.i))
            self.i += 1
            reward+=1

        # if distance_between<0.5: print("waypoint reached")
        # print("distance travelled",self.distance_traveled)
        self.previous_location = self.transform.location
        if self.episode_start + SECONDS_PER_EPISODE < time.time():  # if episode doesnt complete within 10 seconds, then terminate the episode
            done = True
        reward_dist = np.clip(self.prev_distance - self.current_dist, -10.0, 10.0)

        # Get angle difference between closest waypoint and vehicle forward vector
        fwd = vector(self.vehicle.get_velocity())
        wp_fwd = vector(self.current_waypoint.transform.rotation.get_forward_vector())
        angle = angle_diff(fwd, wp_fwd)

        speed_kmh = 3.6 * self.speed
        if speed_kmh < min_speed:  # When speed is in [0, min_speed] range
            speed_reward = speed_kmh / min_speed  # Linearly interpolate [0, 1] over [0, min_speed]
        elif speed_kmh > target_speed:  # When speed is in [target_speed, inf]
            # Interpolate from [1, 0, -inf] over [target_speed, max_speed, inf]
            speed_reward = 1.0 - (speed_kmh - target_speed) / (max_speed - target_speed)
        else:  # Otherwise
            speed_reward = 1.0  # Return 1 for speeds in range [min_speed, target_speed]

        # Interpolated from 1 when centered to 0 when 3 m from center
        centering_factor = max(1.0 - self.distance_from_center / max_distance, 0.0)

        # Interpolated from 1 when aligned with the road to 0 when +/- 20 degress of road
        angle_factor = max(1.0 - abs(angle / np.deg2rad(20)), 0.0)
        distance_between = self.transform.location.distance(self.end_wp.transform.location)
       #print('distance between end location and vehicle current pos',distance_between)
        if distance_between<1:
            reward += 300

        # Final reward
        reward = reward + reward_dist + speed_reward + centering_factor + angle_factor

        return done, reward

    def close(self):
        for i in self.actor_list:
            i.destroy()
        if self.world is not None:
            self.world.destroy()
        self.closed = True


    def step(self, action):
        # 9actions
        self.i = 1
        action = DISCRETE_ACTIONS[int(action)]
        throttle = float(np.clip(action[0], 0, 1))  #  clip is used to clip the interval edges i.e greater than 1 = 1 and lesser than 0 = 0
        brake = float(np.abs(np.clip(action[2], 0, 1)))
        steer = float(np.clip(action[1], -1, 1))
        self.vehicle.apply_control(carla.VehicleControl(throttle=throttle, steer=steer, brake=brake))
        self.prev_distance, angle1 = compute_magnitude_angle(self.start_wp.transform.location, self.vehicle.get_location(),self.vehicle.get_transform().rotation.yaw)
        self.current_dist, angle = compute_magnitude_angle(self.vehicle.get_location(), self.end_wp.transform.location,self.vehicle.get_transform().rotation.yaw)


        # Get vehicle transform
        self.transform = self.vehicle.get_transform()

        # Keep track of closest waypoint on the route
        waypoint_index = self.current_waypoint_index
        for _ in range(len(self.route_waypoints)):
            # Check if we passed the next waypoint along the route
            next_waypoint_index = waypoint_index + 1
            wp, _ = self.route_waypoints[next_waypoint_index % len(self.route_waypoints)]
            dot = np.dot(vector(wp.transform.get_forward_vector())[:2],
                         vector(self.transform.location - wp.transform.location)[:2])
            if dot > 0.0:  # Did we pass the waypoint?
                waypoint_index += 1  # Go to next waypoint
            else:
                break
        self.current_waypoint_index = waypoint_index

        # Check for route completion
        if self.current_waypoint_index < len(self.route_waypoints) - 1:
            self.next_waypoint, self.next_road_maneuver = self.route_waypoints[
                (self.current_waypoint_index + 1) % len(self.route_waypoints)]
        self.current_waypoint, self.current_road_maneuver = self.route_waypoints[
            self.current_waypoint_index % len(self.route_waypoints)]
        self.routes_completed = self.num_routes_completed + (self.current_waypoint_index + 1) / len(
            self.route_waypoints)

        # Calculate deviation from center of the lane
        self.distance_from_center = distance_to_line(vector(self.current_waypoint.transform.location),
                                                     vector(self.next_waypoint.transform.location),vector(self.transform.location))
        #self.center_lane_deviation += self.distance_from_center
        self.center_lane_deviation = self.distance_from_center


        print("center_lane_deviation :",self.center_lane_deviation)

        # Calculate distance traveled

        self.distance_traveled += self.previous_location.distance(self.transform.location)
        """
        distance_between = self.previous_location.distance(self.waypoint_list[self.i])
        if distance_between<0.5:
            self.i+=0
        
        #if distance_between<0.5: print("waypoint reached")
        #print("distance travelled",self.distance_traveled)
        self.previous_location = self.transform.location
        """
        done, reward = self.reward_function()
        self.total_reward += reward
        self.step_count += 1
        return self.encode(self.front_camera), reward, done, None