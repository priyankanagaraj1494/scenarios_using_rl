# distance between 2 waypoin

import glob
import os
import sys
import math
import random
import time
import numpy as np

try:
    sys.path.append(glob.glob('C:/Users/pnagaraj/Data/carla_versions/CARLA_0.9.5/PythonAPI/carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass

import carla

actor_list = []
def get_transform(vehicle_location, angle, d=6.4):
    a = math.radians(angle)
    location = carla.Location(d * math.cos(a), d * math.sin(a), 2.0) + vehicle_location
    return carla.Transform(location, carla.Rotation(yaw=180 + angle, pitch=-15))

def main1():
    try:
        client = carla.Client('localhost', 2000)
        client.set_timeout(2.0)
        #world = client.get_world()
        world = client.load_world('Town07')
        map = world.get_map()
        spectator = world.get_spectator()
        print(map.name)
        blueprint_library = world.get_blueprint_library()
        model_3 = random.choice(blueprint_library.filter('vehicle.bmw.*'))  # to get tesla model 3
        spawn_point = map.get_spawn_points()[5] #generates fixed start location(1,2)(3,4)()
        #spawn_point = carla.Transform(carla.Location(x=-205.288, y=32.537, z=0), carla.Rotation(pitch=360, yaw=265.553, roll=0))
        vehicle = world.spawn_actor(model_3, spawn_point)
        previous_location = vehicle.get_transform().location# passing the model3 bp and the location to spawn
        actor_list.append(vehicle)
        angle = 90
        while angle < 300:
            timestamp = world.wait_for_tick()
            angle += timestamp.delta_seconds * 60.0
            spectator.set_transform(get_transform(vehicle.get_location(), angle - 90))
        waypoint = map.get_waypoint(vehicle.get_location(), project_to_road=True, lane_type=(carla.LaneType.Driving))
        waypoint_list = map.generate_waypoints(2.0)
        #start_wp, end_wp = [map.get_waypoint(spawn.location) for spawn in np.random.choice(map.get_spawn_points(), 2, replace=False)]
        """ #print statements
        print("waypoint : ",waypoint)
         #print(start_wp,end_wp) #Waypoint(Transform(Location(x=-51.9075, y=-2.83632, z=0.0267639), Rotation(pitch=360, yaw=179.441, roll=0)))
        #print(start_wp.transform.location) #Location(x=5.52894, y=0.103957, z=0.0267639)
        #route_waypoints = compute_route_waypoints(world.map, start_wp, end_wp, resolution=1.0)
        #print("waypoint next:", waypoint.next(2.0)[:2])
        """
        vehicle.set_simulate_physics(False)
        distance_travelled=0
        while True:
        #i=0
        #while(i<50): # to stop the vehicle at certain location
            waypoint = random.choice(waypoint.next(2.0)) #list
            vehicle.set_transform(waypoint.transform)
            #vehicle.set_autopilot(True)
            print(vehicle.get_location())
            distance_travelled = previous_location.distance(waypoint.transform.location)
            previous_location = waypoint.transform.location
            print("distance between 2 points",distance_travelled)
            time.sleep(2)
            world.debug.draw_string(waypoint.transform.location, 'x', draw_shadow=False,
                        color=carla.Color(r=255, g=0, b=0), life_time=10,
                        persistent_lines=True)
            #i+=1

            #time.sleep()
        #time.sleep(10) # to increase the duration


    finally:
        #for i in actor_list:
           #i.destroy()
        print('###########')
        print('done ! ')


if __name__ == '__main__':
   main1()