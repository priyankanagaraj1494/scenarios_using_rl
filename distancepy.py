#to calculate distance travelled

import glob
import os
import sys
import math
import time
try:
    sys.path.append(glob.glob('C:/Users/pnagaraj/Data/carla_versions/CARLA_0.9.5/PythonAPI/carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass

import carla

actor_list = []
def get_transform(vehicle_location, angle, d=6.4):
    a = math.radians(angle)
    location = carla.Location(d * math.cos(a), d * math.sin(a), 2.0) + vehicle_location
    return carla.Transform(location, carla.Rotation(yaw=180 + angle, pitch=-15))

def view_angle(world,vehicle):
    spectator = world.get_spectator()
    angle = 90
    while angle < 300:
        timestamp = world.wait_for_tick()
        angle += timestamp.delta_seconds * 60.0
        spectator.set_transform(get_transform(vehicle.get_location(), angle - 90))

def draw_marker(world, vehicle, seconds):
    world.debug.draw_string(vehicle.get_location(), '*', draw_shadow=False,
                                color=carla.Color(r=255, g=0, b=0), life_time=seconds,
                                persistent_lines=False)

def main():
    try:
        seconds = 80
        client = carla.Client('localhost', 2000)
        client.set_timeout(2.0)
        #world = client.get_world()
        world = client.load_world('Town03')
        map= world.get_map()
        print(map.name)
        blueprint_library = world.get_blueprint_library()
        model_3 = blueprint_library.filter("model3")[0]  # to get tesla model 3
        transform = map.get_spawn_points()[6] #generates fixed start location
        #transform = carla.Transform(carla.Location(x=-135.502, y=139.116, z=1.84328),carla.Rotation(pitch=0, yaw=-0.597992, roll=0))
        vehicle = world.spawn_actor(model_3, transform)
        previous_location = vehicle.get_transform().location
        actor_list.append(vehicle)
        view_angle(world, vehicle)
        print("Starting Autonomous Car")
        distance_travelled = 0
        while True: #put vehicle.set_autopilot(True) inside this loop if Autonomous car running infinetly
            vehicle.set_autopilot(True)
            transform1 = vehicle.get_transform()
            draw_marker(world, vehicle, seconds)
            distance_travelled += previous_location.distance(transform1.location)
            previous_location = transform1.location
            print(distance_travelled)
            time.sleep(0.1) # to increase the duration


    finally:
        for i in actor_list:
            i.destroy()
        print('Destroyed Autonomous Car')


if __name__ == '__main__':
   main()
