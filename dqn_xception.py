#DQN to store q_values. The DQN is an xception model

import glob
import os
import sys
import random
import time
import numpy as np
import cv2
import math
from collections import deque
from keras.applications.xception import Xception
from keras.layers import Dense, GlobalAveragePooling2D
from keras.optimizers import Adam
from keras.models import Model
import tensorflow as tf
import keras.backend.tensorflow_backend as backend
from threading import Thread
from tqdm import tqdm
from keras.callbacks import TensorBoard

try:
    sys.path.append(glob.glob('C:/Users/pnagaraj/Data/carla_versions/CARLA_0.9.5/PythonAPI/carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass
import carla

os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
SHOW_PREVIEW = False # to view the episode, carla screen
IM_WIDTH = 640
IM_HEIGHT = 480
SECONDS_PER_EPISODE = 10
REPLAY_MEMORY_SIZE = 5_000
MIN_REPLAY_REPLAY_MEMORY_SIZE = 1_000
MINIBATCH_SIZE = 16
PREDICTION_BATCH_SIZE = 1
TRAINING_BATCH_SIZE = MINIBATCH_SIZE //4
UPDATE_TARGET_EVERY = 5 #updating target model after 5 episodes
MODEL_NAME = "Xception"
PREDICTION_BATCH_SIZE = 1
# amount of memory consumption
MEMORY_FRACTION = 0.8
MIN_REWARD = -1
#EPISODES = 10_000
DISCOUNT = 0.99
epsilon = 1
EPSILON_DECAY = 0.995 #0.95 0.9975 0.99975
MIN_EPSILON = 0.001
AGGREGATE_STATS_EVERY = 10

class ModifiedTensorBoard(TensorBoard):

    # Overriding init to set initial step and writer (we want one log file for all .fit() calls)
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.step = 1
        self.writer = tf.summary.FileWriter(self.log_dir)

    # Overriding this method to stop creating default log writer
    def set_model(self, model):
        pass

    # Overrided, saves logs with our step number
    # (otherwise every .fit() will start writing from 0th step)
    def on_epoch_end(self, epoch, logs=None):
        self.update_stats(**logs)

    # Overrided
    # We train for one batch only, no need to save anything at epoch end
    def on_batch_end(self, batch, logs=None):
        pass

    # Overrided, so won't close writer
    def on_train_end(self, _):
        pass

    # Custom method for saving own metrics
    # Creates writer, writes custom metrics and closes writer
    def update_stats(self, **stats):
        self._write_logs(stats, self.step)

def get_transform(vehicle_location, angle, d=6.4):
    a = math.radians(angle)
    location = carla.Location(d * math.cos(a), d * math.sin(a), 2.0) + vehicle_location
    return carla.Transform(location, carla.Rotation(yaw=180 + angle, pitch=-15))

#car environment--CARLA
class CarEnv:
    SHOW_CAM= SHOW_PREVIEW
    STEER_AMT= 1.0
    im_width= IM_WIDTH
    im_height = IM_HEIGHT
    front_camera = None

    def __init__(self):
        self.client = carla.Client("127.0.0.1",2000) #connect to server
        self.client.set_timeout(5.0)
        self.world = self.client.get_world()
        self.blueprint_library = self.world.get_blueprint_library()
        self.spectator = self.world.get_spectator()

    # begining of the environment, every episode to reset
    def reset(self):
        self.collison_hist = [] #to reset/clear
        self.actor_list  = [] #to reset/clear

        #vehicle-actor
        self.model_3 = self.blueprint_library.filter("model3")[0] # to get tesla model 3
        self.transform = random.choice(self.world.get_map().get_spawn_points())
        self.vehicle = self.world.spawn_actor(self.model_3, self.transform) # passing the model3 bp and the location to spawn
        self.actor_list.append(self.vehicle)

        #sensor
        self.rgb_cam = self.blueprint_library.find('sensor.camera.rgb')
        self.rgb_cam.set_attribute("image_size_x", f"{self.im_width}")
        self.rgb_cam.set_attribute("image_size_y", f"{self.im_height}")
        self.rgb_cam.set_attribute("fov", f"120")
        transform = carla.Transform(carla.Location(x=2.5, z=0.7))
        self.sensor = self.world.spawn_actor(self.rgb_cam, transform, attach_to = self.vehicle)
        self.actor_list.append(self.sensor)
        self.sensor.listen(lambda data: self.process_img(data))

        self.vehicle.apply_control(carla.VehicleControl(throttle= 0.0, brake = 0.0))
        time.sleep(4)

        #collision sensor
        colsensor = self.blueprint_library.find("sensor.other.collision")
        self.colsensor = self.world.spawn_actor(colsensor, transform, attach_to = self.vehicle)
        self.actor_list.append(self.colsensor)
        self.colsensor.listen(lambda event: self.collision_data(event)) #just record the collision events

        while self.front_camera is None:
            time.sleep(0.01)

        self.episode_start = time.time() # save the start time of the episode
        self.vehicle.apply_control(carla.VehicleControl(throttle= 0.0, brake= 0.0))
        angle = 90
        while angle < 280:
            timestamp = self.world.wait_for_tick()
            angle += timestamp.delta_seconds * 60.0
            self.spectator.set_transform(get_transform(self.actor_list[0].get_location(), angle - 90))


        return self.front_camera

    def collision_data(self,event):
        self.collison_hist.append(event)


    def process_img(self, image):
        i=np.array(image.raw_data)
        i2= i.reshape((self.im_height,self.im_width,4))
        i3= i2[:,:,:3]
        if self.SHOW_CAM: #if true it will show thw images captured by camera
            cv2.imshow("",i3)
            cv2.waitKey(1)
        self.front_camera =i3

    def step(self, action):
        if action == 0:
            self.vehicle.apply_control(carla.VehicleControl(throttle = 1.0, steer= 0) )#go straight
        if action == 1:
            self.vehicle.apply_control(carla.VehicleControl(throttle = 1.0, steer= -1 * self.STEER_AMT)) #go left
        if action == 2:
            self.vehicle.apply_control(carla.VehicleControl(throttle = 1.0, steer= 1 * self.STEER_AMT)) #go right

        v = self.vehicle.get_velocity()
        kmh = int(3.6 * math.sqrt(v.x**2 + v.y**2 + v.z**2))

        #reward calculation based on collision, speed
        if len(self.collison_hist)!= 0:  #any collison terminate the episode
            done= True
            reward = -1 #-200

        elif kmh< 50: #speed # low speed, gets penalty
            done = False
            reward = -0.005 #-1

        else:
            done = False
            reward = 0.005 #1

        if self.episode_start + SECONDS_PER_EPISODE < time.time(): # if episode doesnt complete within 10 seconds, then terminate the episode
            done = True

        return self.front_camera, reward, done, None


#class for our agent
class DQNAgent:
    def __init__(self):
        #training model
        self.model = self.create_model()
        #target model updating every step
        self.target_model = self.create_model()
        self.target_model.set_weights(self.model.get_weights())
        #memory of size 5000
        self.replay_memory = deque(maxlen= REPLAY_MEMORY_SIZE) #deque allows adding and removing elements-memory of previous actions
        #to save logs in folder logs/Xception-time
        self.tensorboard = ModifiedTensorBoard(log_dir = f"logs/{MODEL_NAME}- {int(time.time())}")
        self.target_update_counter = 0
        self.graph = tf.get_default_graph()

        self.terminate = False
        self.last_logged_episode = 0
        self.training_initialized = False

        #model can be changed
    def create_model(self): #takes state as input and returns q_values for all action
        base_model = Xception(weights=None, include_top=False, input_shape=(IM_HEIGHT, IM_WIDTH, 3))
        x = base_model.output
        x = GlobalAveragePooling2D()(x)
        predictions = Dense(3, activation = "linear")(x)

        model = Model(inputs = base_model.input, outputs = predictions)
        model.compile(loss= "mse", optimizer = Adam(lr = 0.001), metrics = ["accuracy"])
        return model

    def update_replay_memory(self, transition): #saving all the transitions in replay memory- transitions required to train model
        #transition = (current_state, action, reward, new_state, done)
        return self.replay_memory.append(transition)

    def train(self):
        if len(self.replay_memory)< MIN_REPLAY_REPLAY_MEMORY_SIZE: #1000
            #print("replay memory is less than 1000, so returning:exit")
            return

        print(f"\n replay_memory_size: {len(self.replay_memory)}")
        minibatch = random.sample(self.replay_memory, MINIBATCH_SIZE) #(memory,16)-select random 16 samples
        current_states = np.array([transition[0] for transition in minibatch])/255 #current state is image so /255 normalizing
        with self.graph.as_default():
            #given current_state i.e the image as input and batch_size =1, it gives output action
            #predicting for the recorded transition
            current_qs_list = self.model.predict(current_states, PREDICTION_BATCH_SIZE)
            #print("current_qs_list: actions::",current_qs_list) #outputs q_values of actions

        new_current_states = np.array([transition[3] for transition in minibatch])/255
        with self.graph.as_default():
            future_qs_list = self.target_model.predict(new_current_states, PREDICTION_BATCH_SIZE)

        X=[]
        Y=[]

        #finidng q value for each state using equation(new_q = reward+ discount*max(future_q)
        for index, (current_state, action, reward, new_state, obs) in enumerate(minibatch):
            if not done:
                max_future_q = np.max(future_qs_list[index]) #q_value
                new_q = reward + DISCOUNT * max_future_q
            else:
                new_q = reward

            current_qs = current_qs_list[index]
            current_qs[action]= new_q

            X.append(current_state)
            Y.append(current_qs)

        #values for tensorboard
        log_this_step = False
        if self.tensorboard.step > self.last_logged_episode: #(1>0)
            log_this_step = True
            self.last_logged_episode = self.tensorboard.step
        with self.graph.as_default():#batch-size =4
            self.model.fit(np.array(X)/255, np.array(Y), batch_size = TRAINING_BATCH_SIZE, verbose = 0, shuffle = False, callbacks =[self.tensorboard] if log_this_step else None)

        if log_this_step:
            self.target_update_counter +=1 #used to log for every 5 episodes

        if self.target_update_counter > UPDATE_TARGET_EVERY:
            self.target_model.set_weights(self.model.get_weights())
            self.target_update_counter = 0

    def get_qs(self, state):
            return self.model.predict(np.array(state).reshape(-1, *state.shape)/255)[0] #unpacking h*w*3 from state

    def train_in_loop(self):
            #fit for nothing, like not real data
            X = np.random.uniform(size = (1, IM_HEIGHT, IM_WIDTH, 3)).astype(np.float32) #random array of size(640,480,3)
            Y = np.random.uniform(size =(1,3)).astype(np.float32) #random 3 numbers [[va1,val2,val3]]
            with self.graph.as_default():
                #training. X-input, Y-output
                self.model.fit(X,Y, verbose = False, batch_size =1)

            self.training_initialized = True

            while True:
                if self.terminate:
                    return
                self.train()
                time.sleep(0.01)

if __name__ =='__main__':

    FPS = 80
    ep_rewards = [-10]

    #for repetability
    random.seed(1) #generate same random number
    np.random.seed(1)
    tf.set_random_seed(1)#To make the random sequences generated by all ops be repeatable across sessions, set a graph-level seed:

    #incase multiple agents:::
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction = MEMORY_FRACTION)
    #Sets the global TensorFlow session.
    backend.set_session(tf.Session(config=tf.ConfigProto(gpu_options = gpu_options)))

    if not os.path.isdir("models"):
        os.makedirs("models")

    agent = DQNAgent()
    env = CarEnv()
    #Using threads allows a program to run multiple operations concurrently in the same process space.
    #instantiating thread with target function and call start() to begin working, daemon doesnt have to wait for all threads to finish
    trainer_thread = Thread(target = agent.train_in_loop, daemon = True) #train and predict simultaneosuly
    trainer_thread.start()

    #until agent.training_init is True it waits
    while not agent.training_initialized:
        time.sleep(0.01)

    agent.get_qs(np.ones((env.im_height,env.im_width, 3)))

    """DURING EPISODE; updating REPLAY MEM, predict and train """
    for episode in tqdm(range(1, EPISODES +1), ascii= True, unit = "episodes"): #ascii and unit are for display w.r.t tqdm
        env.collision_hist = []
        agent.tensorboard.step = episode
        episode_reward = 0
        step = 1
        current_state = env.reset()
        done = False
        episode_start = time.time()

        while True: #while not done
            if np.random.random()> epsilon:
                action = np.argmax(agent.get_qs(current_state)) #exploit
            else:
                action = np.random.randint(0,3) #explore
                time.sleep(1/FPS)

            new_state, reward, done, _ = env.step(action)
            episode_reward += reward

            #is current state always reset??
            agent.update_replay_memory((current_state, action, reward, new_state, done)) #adding each transition to the replay-memory of size 5000

            step += 1

            if done:
                break

        for actor in env.actor_list:
            actor.destroy()

        #*****************************************************************************#
        # Append episode reward to a list and log stats (every given number of episodes)
        ep_rewards.append(episode_reward)
        if not episode % AGGREGATE_STATS_EVERY or episode == 1:
            average_reward = sum(ep_rewards[-AGGREGATE_STATS_EVERY:])/len(ep_rewards[-AGGREGATE_STATS_EVERY:])
            min_reward = min(ep_rewards[-AGGREGATE_STATS_EVERY:])
            max_reward = max(ep_rewards[-AGGREGATE_STATS_EVERY:])
            agent.tensorboard.update_stats(reward_avg=average_reward, reward_min=min_reward, reward_max=max_reward, epsilon=epsilon)

            # Save model, but only when min reward is greater or equal a set value
            if average_reward >= MIN_REWARD: #can change to avg model or whatever we need
                agent.model.save(f'models/{MODEL_NAME}__{max_reward:_>7.2f}max_{average_reward:_>7.2f}avg_{min_reward:_>7.2f}min__{int(time.time())}.model')

        # Decay epsilon
        if epsilon > MIN_EPSILON:
            epsilon *= EPSILON_DECAY
            epsilon = max(MIN_EPSILON, epsilon)

    # Set termination flag for training thread and wait for it to finish
    agent.terminate = True
    trainer_thread.join()
    #agent.model.save(f'models/{MODEL_NAME}_{max_reward:_>7.2f}max_{average_reward:_>7.2f}avg_{min_reward::_>7.2f}min_{int(time.time())}.model')
    agent.model.save(f'models/{MODEL_NAME}_{max_reward}max_{average_reward}avg_{min_reward}min_{int(time.time())}.model')