#vehicle takes in the waypoint and teleports  the vehicle to the waypoint locations
import glob
import os
import sys
import math
import random
import time
from PythonAPI.carla.agents.tools.misc import draw_waypoints

try:
    sys.path.append(glob.glob('C:/Users/pnagaraj/Data/carla_versions/CARLA_0.9.5/PythonAPI/carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass

import carla

actor_list = []
waypoint_l = []
def get_transform(vehicle_location, angle, d=6.4):
    a = math.radians(angle)
    location = carla.Location(d * math.cos(a), d * math.sin(a), 2.0) + vehicle_location
    return carla.Transform(location, carla.Rotation(yaw=180 + angle, pitch=-15))

def view_angle(world,vehicle):
    spectator = world.get_spectator()
    angle = 90
    while angle < 300:
        timestamp = world.wait_for_tick()
        angle += timestamp.delta_seconds * 60.0
        spectator.set_transform(get_transform(vehicle.get_location(), angle - 90))

def main():
    try:
        client = carla.Client('localhost', 2000)
        client.set_timeout(2.0)
        # world = client.get_world()
        world = client.load_world('Town07') # to set map
        map = world.get_map()
        print(map.name)
        blueprint_library = world.get_blueprint_library()
        model_3 = random.choice(blueprint_library.filter('vehicle.bmw.*'))
        spawn_point = map.get_spawn_points()[5]  # generates fixed start location(1,2)(3,4)()
        vehicle = world.spawn_actor(model_3, spawn_point)  # passing the model3 bp and the location to spawn
        actor_list.append(vehicle)
        view_angle(world,vehicle)
        vehicle.set_simulate_physics(False)
        #while True: #always while == True
        i = 0
        while(i < 80):
            waypoint = map.get_waypoint(vehicle.get_location(), project_to_road=True, lane_type=(carla.LaneType.Driving))
            waypoint = random.choice(waypoint.next(2.0))
            vehicle.set_transform(waypoint.transform)
            waypoint_l.append(waypoint)
            time.sleep(0.1)
            world.debug.draw_string(waypoint.transform.location, 'x', draw_shadow=False,
                                  color=carla.Color(r=255, g=190, b=0), life_time=10,
                                  persistent_lines=True)
            i+=1
        #draw_waypoints(world, waypoint_l, z=0.5)# to draw after the termination
        #time.sleep(5)

    finally:
        for i in actor_list:
            i.destroy()
        print('###########')
        print('done ! ')


if __name__ == '__main__':
    main()